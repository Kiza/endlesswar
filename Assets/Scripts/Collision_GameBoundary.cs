﻿using UnityEngine;

public class Collision_GameBoundary : MonoBehaviour
{
    // when a GameObject leaves this collider, remove it from the game
    void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }
}
