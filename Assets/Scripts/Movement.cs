﻿[System.Serializable] // Allows Unity inspector to display this sub-class's public properties
public class Movement
{
    public float speed = 8, tilt = 30, xMin = -5.5f, xMax = 5.5f, yMin,yMax, zMin, zMax;
}