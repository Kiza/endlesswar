﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GameController : NetworkBehaviour {
    public GameObject[] enemyPrefabs;
    public Vector3 enemySpawnPoint;
    public int enemiesPerWave;
    public int playerHealth = 0;
    public float spawnDelay;
    public float startDelay;
    public float waveDelay;
    public GameObject playerExplosion;

    public UnityEngine.UI.Text scoreText;
    public UnityEngine.UI.Text healthText;
    public UnityEngine.UI.Text restartText;
    public UnityEngine.UI.Text gameOverText;
    public UnityEngine.UI.Text debugText;

    [HideInInspector] public GameState gameState;
    private int score;
    private GameObject[] player;
    private GameObject gameArea;

    public enum GameState {
        gameOver,
        running,
        paused,
        alternate
    } 

    void Start()
    {
        gameState = GameState.paused;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        UpdateHealth();
        StartCoroutine(SpawnWaves());

    }

    void Update()
    {
        if (gameState == GameState.gameOver)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                FindObjectOfType<NetworkManager>().ServerChangeScene("Game");
                //Application.LoadLevel(Application.loadedLevel);
            }
        }

        /*
        string str = "";
        player = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject ps in player)
        {
            str += "\nPlayer: " + ps.GetComponent<Transform>().position;
        }

        Transform[] tr = gameArea.GetComponentsInChildren<Transform>();
        foreach (Transform trans in tr)
        {
            str +=  "\n"+ "GameArea: " + trans.position;
        }
        debugText.text = str;*/
    }

    // runs as a co-routine to allow for a wait between waves of spawns
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startDelay);
        while (true)
        {
            for (int i = 0; i < enemiesPerWave; i++)
            {
                GameObject hazard = enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-enemySpawnPoint.x, enemySpawnPoint.x), enemySpawnPoint.y, enemySpawnPoint.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnDelay);
            }
            yield return new WaitForSeconds(waveDelay);

            if (gameState == GameState.gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                //restart = true;
                break;
            }
        }
    }

    public void DamagePlayer(int damage)
    {
        playerHealth -= damage;
        if (playerHealth <= 0)
        {
            playerHealth = 0;
            GameOver();
        }
        UpdateHealth();
    }


    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateHealth()
    {
        healthText.text = "Health: " + playerHealth;
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Instantiate(playerExplosion, player.transform.position, player.transform.rotation);
        Destroy(player.gameObject);
        gameOverText.text = "Game Over!";
        gameState = GameState.gameOver;
    }
}
