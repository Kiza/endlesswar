﻿using UnityEngine;
using System.Collections;

public class Collision_EnemyLaser : MonoBehaviour {
    public GameObject hitExplosion;
    public int damage;
    private GameController gameController;
    private Transform trans;
    void Start()
    {
        trans = GetComponent<Transform>();
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
        {
           return;
        }
        //Instantiate(hitExplosion, other.transform.position, other.transform.rotation);
        Instantiate(hitExplosion, trans.transform.position, trans.transform.rotation);
        gameController.DamagePlayer(damage);
        //Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
