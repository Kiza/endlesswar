﻿using UnityEngine;
using System.Collections;

public class BackgroundScroll : MonoBehaviour {

	public float backgroundScrollSpeed = 0.5f;

    private Renderer bgRenderer;
    private Vector2 textureOffset;

	void Start () {
        bgRenderer = GetComponent<Renderer>();
        // get a reference to the current Texture offset on the first material for the renderer attached to this gameObject
        // "_MainTex" refers to the main texture for this material
        textureOffset = bgRenderer.material.GetTextureOffset("_MainTex");
	}
	
	void Update () {
        // loop the texture offset between 0 and 1 according to current game time x scroll speed
        float y = Mathf.Repeat(backgroundScrollSpeed * Time.time,1);
        Vector2 offset = new Vector2(textureOffset.x, y);
        bgRenderer.material.SetTextureOffset("_MainTex", offset);                	
	}

    // return the texture offset to it's starting values when script is disabled
    void onDisable()
    {
        bgRenderer.material.SetTextureOffset("_MainTex", textureOffset);
    }
}
