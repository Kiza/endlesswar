﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public GameObject projectilePrefab;
    public Transform[] projectileSpawns;

    public float fireRate = 0;
    public float startDelay = 0;
    
	void Start () {
        InvokeRepeating("Shoot", startDelay, fireRate);
	}
	
    void Shoot()
    {
        foreach (Transform spawn in projectileSpawns)
        {
            // fixes to stop projectiles coming off the y-plane when the enemy ship is banking
            // - set y to 0.0f in the instantiation point
            // - explicitly set the rotation to counter for the tilt 
            Instantiate(projectilePrefab, new Vector3(spawn.position.x, 0.0f, spawn.position.z), Quaternion.Euler(0.0f,-180f,0.0f));
        }
    }
}
