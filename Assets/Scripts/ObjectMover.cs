﻿using UnityEngine;

public class ObjectMover : MonoBehaviour {
    public float speed = 0, rotation = 0;
    void Start() {
        // transform.forward refers to the z-axis, (x-axis is transform.right and y-axis is transform.up)
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0f, 0f, rotation/10);
    }
}
