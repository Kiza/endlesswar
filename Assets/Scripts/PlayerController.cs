﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {
    #region Public Members
    //movement
    public Movement movement;

    // weapons
    public float firingRate = 0;
    public Transform[] projectileSpawns;
    public GameObject projectilePrefab;
    #endregion

    #region Private Members
    private Rigidbody rigidBody;
    private float canFire = 0;
    private int nextWeapon = 0;
    #endregion
    
    #region Unity Functions
    void Start () {
        if (!isLocalPlayer)
        {
            GetComponent<Renderer>().enabled = false;
        }
        rigidBody = GetComponent<Rigidbody>();
	}
	
	void Update () {
        // use the Unity Input Manager functionality for keymapping and detection
        if (Input.GetButton("Fire1")  && canFire < Time.time && isLocalPlayer)
        {
            // the time in game time that the next shot is allowed to occur
            canFire = Time.time + firingRate;
            // fire a shot
            Shoot();
        }
    }

    // Since using built in physics engine for movement, need to update this 
    // through FixedUpdate (runs at a set interval) rather than Update (runs each frame)
    void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Movement();
    }
    #endregion

    #region Player Functions
    void Movement()
    {
        // use the Unity Input Manager functionality for keymapping and detection
        float x = Input.GetAxis("Horizontal") ;
        float z = Input.GetAxis("Vertical") ;

        // set new velocity of the player
        Vector3 newVelocity = new Vector3(x * movement.speed, 0, z * movement.speed);
        rigidBody.velocity = newVelocity;

        // rotate the model corresponding to x-axis input (note negative tilt)
        rigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, x * -movement.tilt);

        // make sure player can't go outside screen boundary
        rigidBody.position = new Vector3
        (
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, movement.xMin, movement.xMax),
            0.0f,
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, movement.zMin, movement.zMax)
        );

    }

    void Shoot()
    {
        Instantiate(projectilePrefab, projectileSpawns[nextWeapon++].position, Quaternion.identity);
        // alternate the spawn point of each shot
        nextWeapon = nextWeapon % projectileSpawns.Length; 
    }
    #endregion
}
