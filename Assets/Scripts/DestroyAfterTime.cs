﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

    public float lifetime = 0;
    
	void Start () {
        Destroy(gameObject, lifetime);
	}
	
}
