﻿using UnityEngine;
using System.Collections;

public class MenuDrift : MonoBehaviour {

    public Movement movement;
    public float interval;
    private float nextMove =0,drift,angularDrift,vDrift,aDrift;
    private Rigidbody ship;


    void Start()
    {
        ship = GetComponent<Rigidbody>();

        drift = movement.speed / 10;
        angularDrift = movement.tilt / 10;
        vDrift = Random.Range(-drift, drift);
        aDrift = Random.Range(-angularDrift, angularDrift);
    }

    void FixedUpdate()
    {
        if (Time.time > nextMove)
        {

            ship.velocity = new Vector3(vDrift, vDrift, vDrift);

            ship.angularVelocity = new Vector3(aDrift, 0, aDrift);

            nextMove = Random.Range(interval,interval*2.5f) + Time.time;
            vDrift = Random.Range(-drift, drift);
            aDrift = Random.Range(-angularDrift, angularDrift);


        }

        ship.position = new Vector3(
            Mathf.Clamp(ship.position.x, movement.xMin, movement.xMax),
            Mathf.Clamp(ship.position.x, movement.yMin, movement.yMax),
            Mathf.Clamp(ship.position.z, movement.zMin, movement.zMax)
            );
    }
}
